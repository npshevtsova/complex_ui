using Core.Ui;
using Entities;
using Entities.Builders;
using Forms.Pages;
using NUnit.Framework;
using Week13.ComplexUI.Forms.Pages;

namespace Tests.UI
{

    public class DemoTest
    {
      //TASK 1
        [Test]
        public void TestControl()
        {
            // fix all todo #1 to make test working
            Browser.Navigate("https://www.telerik.com/");
            new HomePage().GoToDevCraftTrial();
        }

        [Test]
        public static void TestSwitch()
        {
            // fix all todo #2 to make test working
            Browser.Navigate("https://www.telerik.com/kendo-vue-ui/components/inputs-wrapper/examples/switch/basic/?theme=default-main&themeVersion=5.0.1");
            new SwitchPage().TurnSwitchOn();
        }

        [Test]
        public void TestCombobox()
        {
            // fix all todo #3 to make test working
            Browser.Navigate("https://www.telerik.com/kendo-vue-ui/components/dropdowns/examples/overview/?theme=default-main&themeVersion=5.0.1");
            new ComboboxPage().SelectSport("Tennis");
        }
        
        [Test]
        public void TestComboboxV2()
        {
            // fix all todo #4 to make test working
            Browser.Navigate("https://www.telerik.com/kendo-vue-ui/components/dropdowns/examples/overview/?theme=default-main&themeVersion=5.0.1");
            new ComboboxPage().SelectSportV2("Tennis");
        }

        [Test]
        public void TestListbox()
        {
            // fix all todo #5 to make test working, so method prints to console the all the employees (Steven White, Nancy King and etc.)
            Browser.Navigate("https://www.telerik.com/kendo-react-ui/components/listbox/examples/overview/func/?theme=default-main&themeVersion=5.0.1");
            new ListboxPage().ShowEmployees();
        }

        [Test]
        public void TestPreviewPanes()
        {
            // fix all todo #6 to make test working
            Browser.Navigate("https://www.telerik.com/support/demos");
            DemosPage demosPage = new DemosPage();
            demosPage.PaneKendoUi.ShowSubItems();
            demosPage.PaneTelerikUiForBlazor.ShowSubItems();
        }

        [Test]
        public static void TestGrid()
        {
            // fix all todo #7 to make test working, test should print to console something like "first product with category Condiments and price 22: Chef Anton's Cajun Seasoning"
            Browser.Navigate("https://www.telerik.com/kendo-react-ui/components/grid/examples/get-started/func/?theme=default-main&themeVersion=5.0.1");
            new GridPage().ShowNameOfProductByCategoryAndPrice("Condiments", 22);
        }
        
        [Test]
        public void TestForm()
        {
            // fix all todo #8 to make test working
            Browser.Navigate("https://www.telerik.com/kendo-react-ui/components/form/examples/basic/func/?theme=default-main&themeVersion=5.0.1");
            User user = UserBuilder.Get();
            new UserPage().CreateUser(user);
        }
        //TASK 2
       //You should write tests for pages here

        // Setup and Teardown should not be in this class, they should be moved to some BaseTest class
        [SetUp]
        public void SetUp()
        {
            Browser.Start();
        }
        
        [TearDown]
        public void TearDown()
        {
            Browser.Quit();
        }
    }
}
