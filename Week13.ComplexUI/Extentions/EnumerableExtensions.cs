﻿using System;
using System.Collections.Generic;

namespace Week13.ComplexUI.Extensions;

public static class EnumerableExtensions
{
    public static int FindIndex<T>(this IEnumerable<T> source, Predicate<T> condition)
    {
        var currentIndex = 0;
        foreach (var item in source)
        {
            if (condition(item))
            {
                return currentIndex;
            }

            ++currentIndex;
        }

        return -1;
    }
}