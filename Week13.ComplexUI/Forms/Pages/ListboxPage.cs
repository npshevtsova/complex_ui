using System;
using System.Linq;
using Core.Ui;

namespace Forms.Pages
{
    public class ListboxPage : BasePage
    {
        // pay attention: not only collection of controls can be used, but collection of any other inherited type 
        private ElementsCollection<BaseElement> Listboxes = new ElementsCollection<BaseElement>("//span[@class='k-list-item-text']");

        public void ShowEmployees()
        {
            // todo #5: implement
            foreach (var employees in Listboxes.GetText())
            {
                Console.WriteLine(employees);
            }
        }
    }
}