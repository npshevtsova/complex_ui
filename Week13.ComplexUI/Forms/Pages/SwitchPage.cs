using Forms.Components;

namespace Forms.Pages
{
    public class SwitchPage
    {
        private readonly Switch Switch = new Switch("//span[@role='switch']", "Switch");

        public void TurnSwitchOn()
        {
            Switch.TurnOn();
        }
    }
}