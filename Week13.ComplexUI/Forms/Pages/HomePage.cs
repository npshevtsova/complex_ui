using Core.Ui;

namespace Forms.Pages
{
    public class HomePage : BasePage
    {
        private readonly BaseElement DevCraftTrialButton = new BaseElement("//a[@href='/try/devcraft-ultimate']", "Download DevCraft Trial");

        public void GoToDevCraftTrial()
        {
            DevCraftTrialButton.Click();
        }
    }
}