using System;
using System.Collections.Generic;
using Core.Ui;
using Forms.Components.Grid;

namespace Forms.Pages
{
    public class GridPage : BasePage
    {
        // here we are using the Grid class directly,
        // but there should be the other class (e.g. ProductsGrid) inherited from Grid
        // it will describe the behavior and data, that is specific for particular data (e.g. Column names)
        private readonly Grid Grid = new Grid("//div[@role='grid']", "Products");

        // arguments of this method are primitive types (string, int)
        // but the object of class Product should be passed instead, and all the data will be extracted from the Product object
        public void ShowNameOfProductByCategoryAndPrice(string category, int price)
        {
            Dictionary<string, string> conditions = new()
            {
                ["CategoryName"] = category,
                ["Price"] = price.ToString()
            };
            string name = Grid.GetRow(conditions).GetCellAtColumn("Name").GetText();
            Console.WriteLine($"first product with category {category} and price {price}: {name}");
        }
    }
}
