using Core.Ui;
using Entities;
using OpenQA.Selenium;

namespace Forms.Pages
{
    public class UserPage : BasePage
    {
        private readonly BaseElement FirstNameInput = new BaseElement("//input[@name='firstName']", "First Name");
        // todo #8: write locators of elements
        private readonly BaseElement LastNameInput = new BaseElement("//input[@name='lastName']", "Last Mame");
        private readonly BaseElement EmailInput = new BaseElement("//input[@name='email']", "Email");
        private readonly BaseElement SubmitButton = new BaseElement("//button[contains(@class, 'k-button')]", "Submit");

        public void CreateUser(User user)
        {
            // todo #8: fill in all the fields with corresponding values from User and click submit button
            FirstNameInput.GetElement().SendKeys(user.FirstName);
            LastNameInput.GetElement().SendKeys(user.LastName);
            EmailInput.GetElement().SendKeys(user.Email);
            SubmitButton.GetElement().Click();
        }
    }
}