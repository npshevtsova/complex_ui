using Core.Ui;
using Forms.Components;

namespace Forms.Pages
{
    public class ComboboxPage : BasePage
    {
        private readonly Combobox SportCombobox = new Combobox("//div[./p[text()='ComboBox']]", "Sport");
        //private readonly ComboboxV2 SportComboboxV2 = new ComboboxV2("//div[./p[text()='ComboBox']]", "Sport");
        private readonly ComboboxV2 SportComboboxV2 = new ComboboxV2("//div[./p[text()='DropDownList']]", "Sport");

        public void SelectSport(string sport)
        {
            SportCombobox.Select(sport);
        }

        public void SelectSportV2(string sport)
        {
            SportComboboxV2.Select(sport);
        }
    }
}