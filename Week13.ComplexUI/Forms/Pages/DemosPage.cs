using Core.Ui;
using Forms.Components;

namespace Forms.Pages
{
    public class DemosPage : BasePage
    {
        public PreviewPane PaneKendoUi = new PreviewPane("Kendo UI");

        // todo #6: initialize this preview pane
        public PreviewPane PaneTelerikUiForBlazor = new PreviewPane("Telerik UI for Blazor");
    }
}