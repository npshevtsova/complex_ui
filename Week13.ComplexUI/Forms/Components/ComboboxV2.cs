using Core.Ui;

namespace Forms.Components
{
    // almost the same as Combobox, but implemented with SearchContext
    public class ComboboxV2 : BaseElement
    {
        // todo #4: replace string.Empty with part of locator
        //private BaseElement OpenCombobox => new BaseElement(".//div[./p[text()='DropDownList']]//span[@class='k-icon k-i-arrow-s']", $"Open combobox {Name}", this);
        private BaseElement OpenCombobox => new BaseElement(".//span[@class='k-icon k-i-arrow-s']", $"Open combobox {Name}", this);

        // todo #4 replace string.Empty with locator, which is base on "option" parameter
        private BaseElement Option(string option) => new BaseElement($"//span[text()= '{option}']", $"option {option} in combobox {Name}");

        public ComboboxV2(string baseXpath, string name) : base(baseXpath, name)
        {
        }

        public void Select(string option)
        {
            OpenCombobox.Click();
            Option(option).Click();
        }
    }
}