using System;
using System.Linq;
using Core.Ui;

namespace Forms.Components
{
    // this is example of reusable components
    // pay attention: elements and methods are not placed directly to the page class, but moved to component classes
    public class PreviewPane : BaseElement
    {
        // todo #6: initialize
        //private ElementsCollection<BaseElement> SubItems => null;
        private ElementsCollection<BaseElement> SubItems => new ElementsCollection<BaseElement>(".//span[contains(@class, 'u-dib')]");
        
        public PreviewPane(string name) 
            : base($"//div[@class='row']/div[contains(@id,'ContentPlaceholder')][./h4[text()='{name}']]", $"Preview pane {name}") 
        {
        }

        public void ShowSubItems()
        {
            Console.WriteLine($"Sub items of {Name}:");
            // todo #6: print all the SubItem names to console (e.g. for "Kendo UI": "Launch UI for jQuery demos", "Launch UI for Angular demos" and etc.)
            var pane = GetElement();
            var subItems = pane.FindElements(Locator)
                .Select(element => $"\"{element.Text}\"");

            Console.WriteLine($"Sub items of {Name}: {string.Join(", ", subItems)}");
        }
    }
}