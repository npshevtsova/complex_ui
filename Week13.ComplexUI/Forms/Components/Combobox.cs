using Core.Ui;

namespace Forms.Components
{
    // as well as switch, this implementation is project-specific: it
    // it means, the class depends on your web-application layout
    // this class can be different for the other web-site
    // in contrast to BaseElement.cs, which is universal for any web-site
    public class Combobox
    {
        public string Name { get; set; }
        private string BaseXpath { get; set; }
        
        // It is supposed, there are a lot of similar ComboBoxes in your application.
        // They have different beginning of locator, but the same internal structure.
        // That's why the first part of locator is set via constructor as BaseXpath.
        // The second part of locator will always be the same.
        // Notes: this is simplified implementation.
        // It's better to combine locators via search context (it allows to find elements inside the others). See ComboboxV2 for implementation
        
        // todo #3 replace string.Empty with correct part of locator
        private BaseElement OpenCombobox => new BaseElement(BaseXpath + "//span[@class='k-icon k-i-arrow-s']", $"Open combobox {Name}");

        // todo #3 replace string.Empty with locator, which is based on "option" parameter
        private BaseElement Option(string option) => new BaseElement("//span[text()='Tennis']", $"option {option} in combobox {Name}");


        public Combobox(string baseXpath, string name)
        {
            Name = name;
            BaseXpath = baseXpath;
        }

        public void Select(string option)
        {
            OpenCombobox.Click();
            Option(option).Click();
        }
    }
}