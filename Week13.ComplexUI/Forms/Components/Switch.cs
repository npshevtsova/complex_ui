using Core.Ui;
using OpenQA.Selenium;
using System;

namespace Forms.Components
{
    public class Switch : BaseElement
    {
        private BaseElement BaseElement { get; set; }

        public Switch(By locator, string name) : base(locator, $"switch {name}")
        {
        }

        public Switch(string baseXpath, string name) : base(baseXpath, $"switch {name}")
        {
        }

        public bool GetState()
        {
            // this is just an example
            // this implementation is specific for particular application.
            // State of switch may be defined based on other attributes and other values.
            // todo #2 and remove next line
            return Browser.Driver.FindElement(Locator).GetAttribute("aria-checked").Equals("true");
        }

        public void TurnOn()
        {
            // todo #2
            SetState(true);
        }

        public void TurnOff()
        {
            SetState(false);
        }

        public void SetState(bool state)
        {
            // todo #2
            if ((GetState() && !state) || (!GetState() && state))
            {
                Click();
            } 
        }
        //public void SetState(bool state)
        //{
        //    // todo #2
        //    if (GetState() != state)
        //    {
        //        Click();
        //    }
        //}
    }
}