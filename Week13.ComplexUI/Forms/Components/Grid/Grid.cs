using System.Collections.Generic;
using System.Linq;
using Core.Ui;
using OpenQA.Selenium;
using Week13.ComplexUI.Extensions;

namespace Forms.Components.Grid
{
    public class Grid : BaseElement
    {
        public string XPath { get; set; }
        
        public Grid(string xpath, string name) : base(xpath, $"{name} grid")
        {
            XPath = xpath;
        }

        private ElementsCollection<GridRow> _rows;
        // todo #7: replace string.Empty with part of locator to find rows inside of the Grid
        public ElementsCollection<GridRow> Rows => _rows ??= new ElementsCollection<GridRow>(".//tr[contains(@class,'k-master-row')]", $"rows of {Name} grid", this);

        private ElementsCollection<BaseElement> _columnNames;
        private ElementsCollection<BaseElement> ColumnNames => _columnNames ??= new ElementsCollection<BaseElement>(".//th/span", $"column names of {Name} grid", this);

        public GridRow GetRow(Dictionary<string, string> conditions)
        {
            return Rows.GetElements().FirstOrDefault(row => row.MatchConditions(conditions));
        }
        
        protected internal int GetColumnNumberByName(string columnName)
        {
            // todo #7: replace "0" with your implementation: read names of all the ColumnNames elements, and find matching
            //var index = ColumnNames.GetElements().FindIndex(column => column.GetText() == columnName);   //when using Extentions methods
            int index = ColumnNames.GetText().ToList().IndexOf(columnName);

            if (index == -1)
            {
                throw new NotFoundException($"Column with name '{columnName}' is not found");
            }
            return index;
        }
    }
}