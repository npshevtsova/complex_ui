using System.Collections.Generic;
using System.Linq;
using Core.Ui;
using OpenQA.Selenium;

namespace Forms.Components.Grid
{
    public class GridRow : BaseElement
    {
        private Grid Grid { get; }
        
        public GridRow(IWebElement webElement, string name, Grid grid) 
            : base(webElement, name, grid)
        {
            Grid = grid;
        }

        private ElementsCollection<GridCell> _cells;
        // todo #7: replace string.Empty with part of locator to rind cells inside of the row

        public ElementsCollection<GridCell> Cells => _cells ??= new ElementsCollection<GridCell>(".//td[@role='gridcell']", "cell", this);

        public GridCell GetCellAtColumn(string columnName)
        {
            return Cells.GetElements().ElementAt(Grid.GetColumnNumberByName(columnName));
        }

        public bool MatchConditions(Dictionary<string, string> conditions)
        {
            return conditions.All(condition => GetCellAtColumn(condition.Key).GetText() == condition.Value);

            //foreach (var (columnName, expectedValue) in conditions)
            //{
            //    if (GetCellAtColumn(columnName).GetText() != expectedValue)
            //    {
            //        return false;
            //    }
            //}
            //return true;
        }
    }
}