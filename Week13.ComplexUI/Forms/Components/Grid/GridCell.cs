using Core.Ui;
using OpenQA.Selenium;

namespace Forms.Components.Grid
{
    public class GridCell : BaseElement
    {
        public GridCell(IWebElement element, string name, GridRow gridRow) : base(element, name, gridRow)
        {
        }
    }
}