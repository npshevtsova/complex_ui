using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;

namespace Core.Ui
{
    // Wrapper for WebDriver and for common actions, performed in the browser.
    public static class Browser
    {
        // This is simplified implementation of Driver.
        // Actually, it can be improved at least in 2 aspects:
        // 1) There is should be browser factory,
        // that allows to initialize different drivers for different browsers (Chrome, Firefox and etc.)  //https://www.toolsqa.com/selenium-webdriver/c-sharp/browser-factory/
        // 2) Current implementation doesn't allow to run multiple tests in parallel (ThreadLocal should be used)
        public static WebDriver Driver { get; private set; }

        // timeout is hardcoded here and below, but it should be extracted from configuration and from settings
        public const int DefaultTimeoutSeconds = 30;

        public static void Start()
        {
            Driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(DefaultTimeoutSeconds);
        }

        public static void Quit()
        {
            Driver.Quit();
            Driver = null;
        }

        public static void Navigate(string url)
        {
            // this is just simplified example of logging. Real logger can be used instead of Console.WriteLine
            Console.WriteLine($"Navigate to url: {url}");

            // todo #1: implement
            Driver.Navigate().GoToUrl(url);
        }

        public static void RefreshPage()
        {
            // ... not implemented
        }
    }
}