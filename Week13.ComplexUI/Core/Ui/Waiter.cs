using System;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace Core.Ui
{
    public static class Waiter
    {
        public static BaseElement WaitForVisible(this BaseElement element, TimeSpan? timeout = null)
        {
            // simplified implementation of Wait.
            // Much more wait-methods will be necessary to implement:
            // WaitForPresent, WaitForAbsent, WaitForInvisible, WaitForEnabled, WaitForDisabled and so on
            timeout ??= TimeSpan.FromSeconds(Browser.DefaultTimeoutSeconds);
            new WebDriverWait(Browser.Driver, timeout.Value) { Message = $"{element.Name} is visible" }
                .Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(element.Locator));
            return element;
        }
    }
}