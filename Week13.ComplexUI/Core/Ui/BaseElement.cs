using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Core.Ui
{
    // wrapper for UI elements
    public class BaseElement
    {
        public By Locator { get; private set; }
        public string Name { get; set; }
        protected BaseElement Parent { get; set; }
        protected IWebElement Element { get; set; }

        // Name can be used for logging, if logging is implemented
        public BaseElement(By locator, string name, BaseElement parent = null)
        {
            Locator = locator;
            Name = name;
            Parent = parent;
        }

        // additional constructor.
        // Can be changed, depending on the locator, that you are using most often.
        // In this particular case, it's supposed xpath is the most used.
        public BaseElement(string baseXpath, string name, BaseElement parent = null)
            : this(By.XPath(baseXpath), name, parent)
        {
        }

        // this constructor will be used specially for ElementsCollection
        public BaseElement(IWebElement element, string name, BaseElement parent = null)
        {
            Element = element;
            Name = name;
            Parent = parent;
        }

        public void Click()
        {
            // this is just simplified example of logging. Real logger can be used instead of Console.WriteLine
            Console.WriteLine($"Click {Name}");

            // todo #1: implement
            GetElement().Click();
            //IWebElement element = Browser.Driver.FindElement(Locator);  // change todo GETELEMeT()
            //element.Click();
        }

        public string GetText()
        {
            // todo #7: replace string.Empty with your implementation
            var text = GetElement().Text;
            Console.WriteLine($"Get text '{text}' From {Name}");
            return text;
        }

        public string GetAttribute(string attributeName)
        {
            return GetElement().GetAttribute(attributeName);
        }

        public void SetText(string text)
        {
            Console.WriteLine($"Set text '{text}' into {Name}");
            IWebElement element = GetElement();
            element.Clear();
            element.SendKeys(text);
        }

        public IWebElement GetElement()
        {
            // this is special case, for Elements, which are initialized via the 3rd constructor
            if (Element != null)
            {
                return Element;
            }
            // todo #4: update search context to make it possible to find elements not only on the whole page, but also inside of the parent (search context) 
            //ISearchContext searchContext = Browser.Driver;
            ISearchContext searchContext = Parent == null ? Browser.Driver : Parent.GetElement();
            return searchContext.FindElement(Locator);

        }
    }
}