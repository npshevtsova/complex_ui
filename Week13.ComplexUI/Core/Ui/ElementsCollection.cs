using System;
using System.Collections.Generic;
using System.Linq;
using Forms.Components.Grid;   //
using OpenQA.Selenium;

namespace Core.Ui
{
    public class ElementsCollection<T> where T : BaseElement
    {
        public By Locator { get; set; }
        public string Name { get; set; }
        public BaseElement Parent { get; set; }
        
        // ElementsCollection allows to wrap multiple elements, specified by only 1 locator
        // and work with them together: extract text of all the elements, click each element and so on...
        public ElementsCollection(string xpath, string name = null, BaseElement parent = null) : this(By.XPath(xpath), name, parent)
        {
        }

        public ElementsCollection(By locator, string name = null, BaseElement parent = null)
        {
            Locator = locator;
            Name = name;
            Parent = parent;
        }

        public IEnumerable<T> GetElements() 
        {
   
            // timeout is hardcoded here and below, but it should be extracted from configuration and from settings
            Browser.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            // todo #5: update search context to make it possible to find elements not only on the whole page, but also inside of the parent (search context) 
            ISearchContext searchContext = Parent == null ? Browser.Driver : Parent.GetElement();
            // todo #5: implement method to return all the elements, that match specified locator 
            return searchContext.FindElements(Locator)
                .Select((element) => 
                Activator.CreateInstance(typeof(T), element, Name, Parent) as T);

            
        }
        public IEnumerable<string> GetText()
        {
            // todo #5: using LINQ implement the method
          return GetElements()
                .Select(element => element.GetText());
   
        }
    }
}